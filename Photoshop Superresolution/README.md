### Shooting for Superresolution

In order to achieve the best results for this technique, it's necessary to shoot a scene with high enough detail. It's also essential that it's a shot of a static scene. Expect any movement in your scene to produce blurry results. As such, this technique is best applied to still landscapes or maybe even studio shots (assuming you're using continuous lighting or that your strobes can recycle fast enough to prevent you from moving too much between shots).

So for your first try, I highly recommend going outside to shoot a highly detailed scene with a lot of distant, static foreground detail.

### Camera Settings

There isn't one best setting for this technique (it depends partially on your equipment) but we should do everything in our power to make every image in our burst as sharp as possible. I recommend shooting with your lens stopped down to f/5.6 to f/11 to maximize sharpness.

Additionally, it's probably best to use a shutter speed that's fast enough to handhold without blur. A good safe guideline for shutter speed is 1/(2*focal length). So if you have a 50mm lens, 1/100th would be a fairly safe shutter speed.

It's also important to use a fairly low ISO so this technique works best in well lit scenes, but it really should just be set as your f/number and shutter speed dictate for a neutral exposure. Auto ISO is helpful in this case.

We'll want to use continuous burst mode as I've already said. I recommend taking at least 20 images. Technically the more images, the better, but 20 is a nice round number and I have found that trying to process more images can really slow down the post processing, even on a high-end computer.

Finally, it's very important that we shoot our photos in RAW so as to maintain the best detail in the shot. When the camera processes JPEGs it often applies noise reduction and smoothing to the image which can reduce our efforts at achieving the best superresolution result. JPEG will work, but RAW will be better.

Handheld

f/5.6 to f/11

Hand holdable shutter speed – 1/(2*focal length) recommended

Lower ISOs are preferable, set as f/number and shutter dictate for a neutral exposure or set Auto ISO

Continuous burst mode – minimum of 20 images

RAW

When shooting, try making several sets of images. Remember that we're not looking for very much hand movement between photos. We need only one pixel of motion between each shot. It's likely that you won't be able to handhold better than one pixel anyway, so just stay as still as you possibly can when firing off your burst of photos.

Check and double check your focus too; it might be possible that your camera may shift focus between photos. If that occurs, switch to manual focus to prevent it from shifting while shooting, but be extra careful to make sure everything is tack sharp before firing away. This method won't work with blurry photos.

### Processing
There's a specific order of operations in processing that will allow us to combine our stack of photos into a final image with noticeably finer detail. We'll import our photos into a stack of layers in Photoshop, upsample the photo (usually 200% width/height) with a simple nearest neighbor algorithm, re-align the layers, and then average the layers together.

Import all photos as stack of layers

Resize image to 4x resolution (200% width/height)

Auto-align layers

Average layers

1. Import the Images as a Stack of Layers

In Photoshop: File>Scripts>Load Files into Stack...

Click Browse... to navigate to your photos

Make sure "Attempt to Automatically Align Source Images" is unchecked (This is essential. If you align first it won't work.)

Click OK

2. Resize to 200% Width/Height

Choose Image>Image Size...

Set Width/Height to 200%

Use the "Nearest Neighbor" resample algorithm. You can also use "Preserve Details" but I prefer "Nearest Neighbor" as it does not oversharpen

Click OK

3. Auto-Align the Layers

Select all the layers in the Layers Palette

Choose Edit>Auto-Align Layers...

Use the "Auto" Projection Setting and uncheck "Geometric Distortion" and uncheck "Vignette Removal"

Click OK

Once aligned, check that each layer looks properly aligned with the bottom layer. If there's one or two that didn't align as well as the others, consider deleting them. You can turn on and off the visibility of each layer with the eye icon to the left of the layer. Just remember to turn them all back on before you continue.

4. Average the Layers

The fastest way to do this is to change the opacity of each layer from bottom to top such that the opacity = 1/(layer number). For example, if you have 20 layers, make the bottom 1/1 = 100%, the second from the top should be 1/2 = 50%, the third 1/3=33%, the fourth 1/4=25% and so on until the top layer which is 1/20 = 5%. Photoshop can only do integer opacities so there will be some rounding error and repeat integers as you get close to the last layer but it won't matter too much.

With 20 layers, opacities from bottom to top are roughly: 100%, 50%, 33%, 25%, 20%, 17%, 14%, 12%, 11%, 10%, 9%, 8%, 8%, 7%, 7%, 6%, 6%, 6%, 5%, 5%
Once opacities are set, select all the layers, right-click and choose Flatten Image

Averaging can also be performed by selecting all the layers and turning them to a Smart Object and setting the Smart Object's stack mode to "Mean" or "Median" but this is can be slow when working with a stack of twenty 90+ megapixel photos just as a warning. The "Median" stack mode is particularly good for removing ghosting in moving objects. Smart Object stack modes are only available in CS6 Extended and CC versions of Photoshop.

### Optional: Apply Smart Sharpen

I usually like to use a smart sharpening filter of about 2px radius and about 200% to 300%. Because of the nature of our method, hard edges will likely look a little soft and will need some sharpening up.

A two-pixel radius works well with our 4x increase in resolution and should keep everything looking natural without noticeable halos. You might find that some alternate settings could work better depending on the content of your photograph.

Filter>Sharpen>Smart Sharpen...

Amount: 300%

Radius: 2px

Reduce Noise: 0%

Click OK

After sharpening, you may want to crop out any extra unfinished edges before saving. That's it! You now have a nearly noise free, super high resolution photo!

