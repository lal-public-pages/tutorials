Fix a movie where the dialogue was almost inaudible, but then all of a sudden an action scene comes in and shakes your house with noise.

## Simple

 - Head to **Tools > Effects and Filters > Audio Effects > Compressor**.
 - Without changing your volume from its usual spot, find a quiet scene in the movie and raise the **Makeup Gain** slider until the volume is at a comfortable level. This will boost the volume of the entire movie so you don't have to change your TV or computer’s volume from its usual setting.
 - Raise the **Ratio** slider all the way up. This will ensure that any sound over a certain volume threshold will be turned down to a level you set.
 - Without changing the volume, find a loud scene in the movie and start playing it. Lower the **Threshold** slider until the sound is at a non-earthquake-inducing level.
 - Lastly, move **Attack** slider up to about 50ms, and **Release** slider up to about 300ms. This makes everything a bit more fluid, so your movie will change volumes when necessary but it will happen a bit more gradually.

## Detailed

Open your movie in the VLC player. Keep the volume to your preferred level.

Go to **Tools > Effects and Filters** and select the **Audio Effects** tab. Under the Audio Effects tab select the **Compressor** tab. Click on the **Enable** checkbox to enable the Compressor setting sliders.

Drag the individual sliders to the values below,

| Option      | Value    |
|-------------|---------:|
| RMS/peak    | 0.1      |
| Attack      | 31.0 ms  |
| Release     | 290.0 ms |
| Threshold   | -23.0 dB |
| Ratio       | 20.0 : 1 |
| Knee Radius | 1.5 dB   |
| Makeup Gain | 10 dB    |

Here is a brief explanation of all the settings.
The most important settings which are responsible for compressing the dynamic range of the sound are, Threshold, Ratio and Makeup Gain.

| Option      | Details |
|-------------|---------|
| RMS/peak    | RMS and peak are the sound detection modes for the compressor. Keep this value at 0 or 0.1 |
| Attack      | This setting sets the interval of time to activate the compressor. The higher this value longer the compressor would take to respond to loud sounds. It accepts a value between 1ms – 50ms. 50ms is about 1/20th of a second. |
| Release     | The fade out speed of the compression. The higher the value the longer the sound compression would remain in effect. |
| Threshold   | The minimum decibel levels to trigger the compressor.  A lower value means that the compressor would be more sensitive to louder sounds. |
| Ratio       | The amount of reduction once the decibel level exceeds the set threshold decibel level. The compressor would make reduction in the exceeded decibel level by the given ratio. |
| Knee Radius | This setting controls the speed of activation of Ratio. You should set it to 1 or 0 but it won’t make any perceivable difference. |
| Makeup Gain | This setting is actually used to compensate for the reduction in overall sound due to compression. This raises the audio level of softer sounds or dialogue. |

You can tweak the settings according to your preference, to get the desired audio level. Moreover, You can disable the Compressor if you want to enjoy the full dynamic audio range of your movie.

